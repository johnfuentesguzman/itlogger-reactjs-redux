import { GET_LOGS, SET_LOADING, ADD_LOGS, DELETE_LOG, LOGS_ERROR, SET_CURRENT, CLEAR_CURRENT, UPDATE_LOG, SEARCH_LOGS } from './types';


export const getLogs = () => async dispatch => {
    try {
        setLoading(); // the idea is have a common function to handle the loading bar
        const res = await fetch('/logs');
        const data = await res.json();
        dispatch({
            type: GET_LOGS,
            payload: data
        });
    } catch (error) {
        dispatch({ type: LOGS_ERROR, payload: error.response.statusText });
    }
};

export const addLog = (log) => async dispatch => {
    try {
        setLoading(); // the idea is have a common function to handle the loading bar
        const res = await fetch('/logs',
            {
                method: 'POST',
                body: JSON.stringify(log),
                headers:
                {
                    'Content-Type': 'Application/json'
                }
            });
        const data = await res.json();
        dispatch({
            type: ADD_LOGS,
            payload: data
        });
    } catch (error) {
        dispatch({ type: LOGS_ERROR, payload: error.response.statusText });
    }
};

export const deleteLog = (logId) => async dispatch => {
    try {
        setLoading(); // the idea is have a common function to handle the loading bar
        await fetch(`/logs/${logId}`,
            {
                method: 'DELETE',
                headers:
                {
                    'Content-Type': 'Application/json'
                }
            });
        dispatch({
            type: DELETE_LOG,
            payload: logId
        });
    } catch (error) {
        dispatch({ type: LOGS_ERROR, payload: error.response.statusText });
    }
};
export const updateLog = (log) => async dispatch => {
    try {
        setLoading(); // the idea is have a common function to handle the loading bar
        const res =  await fetch(`/logs/${log.id}`,
            {
                method: 'PUT',
                body: JSON.stringify(log),
                headers:
                {
                    'Content-Type': 'Application/json'
                }
            });
        const data = await res.json();

        dispatch({
            type: UPDATE_LOG,
            payload: data
        });
    } catch (error) {
        dispatch({ type: LOGS_ERROR, payload: error.response.statusText });
    }
};

export const searchLogs = (text) => async dispatch => {
    try {
        setLoading(); // the idea is have a common function to handle the loading bar
        const res = await fetch(`/logs?q=${text}`);
        const data = await res.json();
        dispatch({
            type: SEARCH_LOGS,
            payload: data
        });
    } catch (error) {
        dispatch({ type: LOGS_ERROR, payload: error.response.statusText });
    }
};

export const setCurrent = (log)  => {
    return {
        type: SET_CURRENT,
        payload: log
    }
};

export const clearCurrent = ()  => {
    return {
        type: CLEAR_CURRENT 
    };
};

export const setLoading = () => { // SET loading to TRUE
    return {
        type: SET_LOADING
    }
};


