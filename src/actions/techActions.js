import { GET_TECHS, ADD_TECH, DELETE_TECH, TECHS_ERROR, SET_LOADING} from './types';

export const getTechs = () => async dispatch => {
    try {
        setLoading(); // the idea is have a common function to handle the loading bar
        const res = await fetch('/techs');
        const data = await res.json();
        dispatch({
            type: GET_TECHS,
            payload: data
        });
    } catch (error) {
        dispatch({ type: TECHS_ERROR, payload: error.response.statusText });
    }
};

export const addTech = (tech) => async dispatch => {
    try {
        setLoading(); // the idea is have a common function to handle the loading bar
        const res = await fetch('/techs',
            {
                method: 'POST',
                body: JSON.stringify(tech),
                headers:
                {
                    'Content-Type': 'Application/json'
                }
            });
        const data = await res.json();
        dispatch({
            type: ADD_TECH,
            payload: data
        });
    } catch (error) {
        console.error(error);
        dispatch({ type: TECHS_ERROR, payload: error.response.statusText });
    }
};

export const deleteTech = (techId) => async dispatch => {
    try {
        setLoading(); // the idea is have a common function to handle the loading bar
        await fetch(`/techs/${techId}`,
            {
                method: 'DELETE',
                headers:
                {
                    'Content-Type': 'Application/json'
                }
            });
        dispatch({
            type: DELETE_TECH,
            payload: techId
        });
    } catch (error) {
        dispatch({ type: TECHS_ERROR, payload: error.response.statusText });
    }
};

export const setLoading = () => { // SET loading to TRUE
    return {
        type: SET_LOADING
    }
};
