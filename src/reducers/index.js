import { combineReducers}  from 'redux';
import logReducer from './logReducer';
import techRedcucer from './techReducer';

export default combineReducers({
    log: logReducer,
    tech: techRedcucer
});