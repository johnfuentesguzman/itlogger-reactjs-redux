import React, { useEffect } from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import TechItem from './TechItem';
import { getTechs } from '../../actions/techActions';

export const TechListModal = ({tech: {techs, loading}, getTechs }) => {
    useEffect(() => {
        getTechs();
        // eslint-disable-next-line
    }, []);


    return (
        <div id='tech-list-modal' className='modal'>
            <div className='modal-content'>
                <h4>Technicians List</h4>
                <ul className='collection'>
                    {!loading && techs !== null  && 
                        techs.map(tech=> <TechItem key={tech.id} tech={tech}/>)
                    }
                </ul>

            </div>
        </div>
    );
}
TechListModal.propTypes = {
    tech: PropTypes.object.isRequired,
    getTechs: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
    tech: state.tech,  // this is part of src/reducers/index.js => "tech: techRedcucer"
}); // if you want to get anything from the state redux&

export default connect ( // connecting our state 'tech' with the action function 'getTechs' and the name of component 'TechListModal'
    mapStateToProps, // getting state
    {getTechs}) // action fuction name 
    (TechListModal);// current componet name