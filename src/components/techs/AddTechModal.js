import React, { useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addTech } from '../../actions/techActions';
import  M from 'materialize-css/dist/js/materialize.js'

export const AddTechModal = ({ addTech }) => {
    const [firstname, setFirstName] = useState('');
    const [lastname, setLastName] = useState('');

    const onSubmit = () => {
        if(firstname === ''|| lastname === ''){
            M.toast({html: 'Please enter the tech name'});
            return;
        }
        const techToSaved = {
            firstname, lastname
        };
        addTech(techToSaved);
        M.toast({ html: 'Technician added'});
        setFirstName(''); // clear input
        setLastName(''); // clear input
    };
    return (
        <div id='add-tech-modal' className='modal'>
            <div className='modal-content'>
                <h4>Enter Systen Logs</h4>
                <div className='row'>
                    <div className='input-field'>
                        <input type='text' name='firstname' value={firstname} onChange={e => setFirstName(e.target.value)} />
                    </div>
                    <label htmlFor='firstname' className='active'>First Name</label>
                </div>
            <div className='row'>
                    <div className='input-field'>
                        <input type='text' name='lastname' value={lastname} onChange={e => setLastName(e.target.value)} />
                    </div>
                    <label htmlFor='lastname' className='active'>Last Name</label>
                </div>
            </div>
            <div className ='modal-footer'>
                <a href='#!' onClick={onSubmit} className='modal-close waves-effect blue  waves-light btn'>Enter</a>
            </div>
        </div>
    )
}

AddTechModal.propTypes = {
    addTech: PropTypes.func.isRequired
};

export default connect // connecting our state 'tech' with the action function 'getTechs' and the name of component 'AddTechModal'
    (null, // getting state this case we not going to show/get data just send it 
    {addTech}) // action fuction name 
    (AddTechModal) ; // current componet name