import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import  M from 'materialize-css/dist/js/materialize.js'
import { deleteTech } from '../../actions/techActions';

export const TechItem = ({ tech, deleteTech}) => {
    const onDelete = () => {
        deleteTech(tech.id);
        M.toast({html: 'Technician Deleted'});
    };

    return (
        <li className='collection-item'>
            <div>
                {tech.firstname} {tech.lastname}
                <a href='#!' className='secondary-content'>
                    <i className='material-icons grey-text' onClick={onDelete}>delete</i>
                </a>
            </div>
        </li>
    )
}

TechItem.propTypes = {
    tech: PropTypes.object.isRequired,
    deleteTech: PropTypes.func.isRequired
}

export default connect 
    (null,
    { deleteTech })
    (TechItem)