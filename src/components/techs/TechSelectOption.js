import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getTechs }  from '../../actions/techActions';

const TechSelectOption = ({ getTechs, tech: {techs, loading }}) => {
    useEffect(() => {
        getTechs();
        // eslint-disable-next-line
    }, [])

    return (
        !loading && techs !== null && techs.map(tech => (
            <option key={tech.id} value={`${tech.firstname} ${tech.lastname} `}>
                {tech.firstname} {tech.lastname}
            </option>)
        )
    )
}

TechSelectOption.propTypes = {
    techs: PropTypes.object.isRequired,
    getTechs: PropTypes.func.isRequired
}

const mapStatesToProps = (state) => ({
    tech: state.tech // this is part of src/reducers/index.js => "tech: techRedcucer"
});

export default connect ( // connecting our state 'tech' with the action function 'getTechs' and the name of component 'TechListModal'
    mapStatesToProps, // getting state
    { getTechs }) // action fuction name 
    (TechSelectOption) // current componet name
