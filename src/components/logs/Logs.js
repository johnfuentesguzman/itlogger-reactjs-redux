import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import LogItem from './LogItem';
import PreLoader from '../layout/PreLoader';
import PropTypes from 'prop-types';
import {getLogs} from '../../actions/logActions';

export const Logs = ({log: {logs, loading}, getLogs }) => { // 'log' parameter comes from  "mapStateToProps". created below
    useEffect(() => {
        getLogs();
        // eslint-disable-next-line
    }, []);

    if(loading || logs === null ){
        return <PreLoader/>;
    };

    return (
        <div>
            <ul className='collection with-header'>
                <li className='collection-header'>
                    <h4 className='center'>System Logs</h4>
                </li>
            </ul>
            {
                !loading && logs && logs.length === 0 ? (<p className='center'>No Logs to show...</p>)
                :
                (logs.map (log => <LogItem log={log} key={log.id}/>)
            )}
        </div>
    );
}
Logs.propTypes = {
    log: PropTypes.object.isRequired,
    getLogs: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    log: state.log // this is part of src/reducers/index.js => "log: logReducer"
}); // if you want to get anything from the state redux

export default connect // connecting our state 'log' with the action function 'getlogs' and the name of component 'Logs'
    (mapStateToProps, // getting state
    {getLogs}) // action fuction name 
    (Logs) ; // current componet name