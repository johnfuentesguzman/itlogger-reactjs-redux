import React, { useEffect, Fragment } from 'react';
import 'materialize-css/dist/css/materialize.min.css';
import M from 'materialize-css/dist/js/materialize.js'
import './App.css';
import SearchBar from './components/layout/SearchBar';
import Logs from './components/logs/Logs';
import AddBtn from './components/layout/AddBtn';
import AddLogModal from './components/logs/AddLogModal'
import AddTechModal from './components/techs/AddTechModal'
import EditLogModal from './components/logs/EditLogModal'
import TechListModal from './components/techs/TechListModal';
import { Provider } from 'react-redux'; // responsible to make react and redux connection
import store from './store';


const App = () => {
  useEffect(() => {
    M.AutoInit(); // init materialize css
  });
  return (
    <Provider store={store}>
      <Fragment>
        <SearchBar />
        <div className='container'>
          <Logs />
          <AddLogModal />
          <EditLogModal />
          <AddTechModal />
          <TechListModal />
          <AddBtn />
        </div>
      </Fragment>
    </Provider>
  );
};

export default App;
